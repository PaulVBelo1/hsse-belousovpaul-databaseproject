-- Создание Таблиц

-- Создание схемы базы данных интернет-магазина рассады
create schema plant_shop_database;

-- Информация о растениях

	create table plant_shop_database.plant (
		plant_id SERIAL primary key,
		plant_name VARCHAR(255) not null,
		plant_sort VARCHAR(255) not null,
		category VARCHAR(255) not null,
		plant_height DOUBLE PRECISION not null check (plant_height > 0),
		sowing_month VARCHAR(255) not null,
		sowing_temp INTEGER not null,
		germination INTEGER not null check (germination > 0),
		harvest VARCHAR(255) not null,
		indoor BOOLEAN not null,
		unique (plant_name, plant_sort)
);


-- Информация о поставщике (СХ)

	create table plant_shop_database.originator (
		originator_id SERIAL primary key,
		originator_name VARCHAR(255) not null unique,
		country VARCHAR(255) not null
);


-- Информация о продавце (рассыпщике)
	
	create table plant_shop_database.supplier (
		supplier_id SERIAL primary key,
		supplier_name VARCHAR(255) not null unique,
		supplier_adress VARCHAR(255) not null,
		supplier_email VARCHAR(255) not null
);

-- Информация о товарах (пакетики семян, саженцев и т.д.)

	create table plant_shop_database.product (
		product_id SERIAL primary key,
		plant_id SERIAL not null,
		supplier_id SERIAL not null,
		originator_id SERIAL not null,
		package_type VARCHAR(255) not null,
		weight DOUBLE PRECISION not null check (weight > 0),
		quantity INTEGER not null check (quantity > 0),
		price numeric(18, 2) not null check (price > 0),
		foreign key (supplier_id) references plant_shop_database.supplier (supplier_id),
		foreign key (originator_id) references plant_shop_database.originator (originator_id)
);


-- Список заказов

	create table plant_shop_database.order (
		order_id SERIAL primary key,
		order_date date not null check (order_date <= NOW()),
		recieve_date date not null, -- Комбинация предполагаемой даты получения и статуса полностью описывает состояние заказа
		status varchar(255) not null,
		order_adress varchar(255) not null,
		client varchar(255) not null
);


-- Таблица-связка "заказ-товар"

	create table plant_shop_database.order_product (
		product_id SERIAL not null,
		order_id SERIAL not null,
		amount INTEGER not null check (amount > 0),
		primary key (product_id, order_id),
		foreign key (product_id) references plant_shop_database.product (product_id),
		foreign key (order_id) references plant_shop_database.order (order_id)
		
);

-- Добавим внешний ключ
alter table plant_shop_database.product
add foreign key (plant_id) 
references plant_shop_database.plant (plant_id)
deferrable initially deferred;
