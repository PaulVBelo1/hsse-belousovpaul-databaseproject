-- Заполнение таблицы "plant"

insert into plant_shop_database.plant
(plant_name, plant_sort, category, plant_height, sowing_month, sowing_temp, germination, harvest, indoor)
values
('Перец сладкий', 'Паланичко чудо', 'Vegetable', 0.7, 'March', 26, 8, 'August', true),
('Перец сладкий', 'Испанский Rico', 'Vegetable', 0.5, 'March', 26, 7, 'August', true),
('Перец сладкий', 'Палермо шоколадный', 'Vegetable', 0.9, 'March', 28, 10, 'August', true),
('Перец сладкий', 'Джипси', 'Vegetable', 0.6, 'March', 26, 7, 'August', true),
('Перец сладкий', 'Lila Liebesapfel', 'Vegetable', 0.5, 'March', 27, 10, 'August', true),
('Перец сладкий', 'Айвар', 'Vegetable', 0.7, 'February', 26, 10, 'August', true),
('Морковь', 'Лагуна', 'Vegetable', 0.25, 'May', 10, 10, 'August', false),
('Лук Репчатый', 'Эксибишен', 'Vegetable', 0.3, 'April', 8, 10, 'June', false),
('Красивотычинник', 'Лимонный', 'Flower', 1, 'any', 20, 10, 'September', true),
('Лобелия многолетняя', 'Старшип Бургунди', 'Flower', 0.7, 'March', 26, 10, 'August', true);


-- Заполнение таблицы "supplier"
insert into plant_shop_database.supplier
(supplier_name, supplier_adress, supplier_email)
values
('ИП Григорьев А.Ю.', 'г. Воронеж, улица Кулибина, 15, корп. 2', 'agromirseeds@mail.ru'),
('Сады России', 'Челябинская обл., Красноармейский район, д. Шибаново, улица Центральная, 92', 'agro@sad-i-ogorod.ru'),
('ООО Агрофирма СемАгро', 'г. Москва, улица Обручева, 55А', 'info@semagro-msw.ru'),
('ИП Мязина', 'Московская обл., г. Лыткарино, улица Советская, 8, корп. 2', 'myazina@myazina.ru'),
('Садовита', 'г. Пемза, улица Перспективная, 9', 'opt@sadovita.ru'),
('ООО Семь Семян', 'г. Москва, Киевское шоссе, 22км, 4', 'info@7semyan.ru'),
('ИП Орлова Н.Д.', 'Московская обл., Щелковский район, д. Гребнево, 24/1А', 'vashisemena@mail.ru'),
('ООО ЛегальнаяТравка', 'г. Москва, Керамический проезд, 31', 'notpodsmoking@mail.ru'),
('ИП Ноунеймин Г.Г.', 'г. Пемза, улица Перспективная, 19', 'fillerdata@idk.edu'),
('Лесновато', 'г. Москва, Киевское шоссе, 59км, 13', 'info@lesnovato.ru');

-- Заполнение таблицы "originator"

insert into plant_shop_database.originator
(originator_name, country)
values
('Serbian Plants', 'Serbia'),
('Greentime', 'Spain'),
('Palermo Chocoloni', 'Italy'),
('Semenis', 'Netherland'),
('Heirloom', 'Germany'),
('Myazina', 'Russia'),
('nunhems', 'Netherland'),
('BEJO ZADEN B.V.', 'Netherland'),
('FlowerItaly', 'Italy'),
('PanAmericanSeed', 'USA');

-- Заполнение таблицы "product"
insert into plant_shop_database.product
(plant_id, supplier_id, originator_id, package_type, weight, quantity, price)
values
((select plant_id from plant_shop_database.plant where plant_name = 'Перец сладкий' and plant_sort='Паланичко чудо'),
(select supplier_id from plant_shop_database.supplier where supplier_name = 'ИП Григорьев А.Ю.'),
(select originator_id from plant_shop_database.originator where originator_name = 'Serbian Plants'),
	'paper pack', 0.1, 30, 74.99
),
((select plant_id from plant_shop_database.plant where plant_name = 'Перец сладкий' and plant_sort='Испанский Rico'),
(select supplier_id from plant_shop_database.supplier where supplier_name = 'ИП Григорьев А.Ю.'),
(select originator_id from plant_shop_database.originator where originator_name = 'Greentime'),
	'paper pack', 0.1, 15, 89.99
),
((select plant_id from plant_shop_database.plant where plant_name = 'Перец сладкий' and plant_sort='Палермо шоколадный'),
(select supplier_id from plant_shop_database.supplier where supplier_name = 'Сады России'),
(select originator_id from plant_shop_database.originator where originator_name = 'Palermo Chocoloni'),
	'paper pack', 0.1, 30, 119.99
),
((select plant_id from plant_shop_database.plant where plant_name = 'Перец сладкий' and plant_sort='Джипси'),
(select supplier_id from plant_shop_database.supplier where supplier_name = 'ООО Агрофирма СемАгро'),
(select originator_id from plant_shop_database.originator where originator_name = 'Semenis'),
	'paper pack', 0.2, 20, 85
),
((select plant_id from plant_shop_database.plant where plant_name = 'Перец сладкий' and plant_sort='Lila Liebesapfel'),
(select supplier_id from plant_shop_database.supplier where supplier_name = 'ИП Григорьев А.Ю.'),
(select originator_id from plant_shop_database.originator where originator_name = 'Heirloom'),
	'paper pack', 0.1, 20, 110
),
((select plant_id from plant_shop_database.plant where plant_name = 'Перец сладкий' and plant_sort='Айвар'),
(select supplier_id from plant_shop_database.supplier where supplier_name = 'ИП Мязина'),
(select originator_id from plant_shop_database.originator where originator_name = 'Myazina'),
	'paper pack', 0.2, 20, 85
), 
((select plant_id from plant_shop_database.plant where plant_name = 'Морковь' and plant_sort='Лагуна'),
(select supplier_id from plant_shop_database.supplier where supplier_name = 'Садовита'),
(select originator_id from plant_shop_database.originator where originator_name = 'nunhems'),
	'paper pack', 0.1, 20, 60
), 
((select plant_id from plant_shop_database.plant where plant_name = 'Лук Репчатый' and plant_sort='Эксибишен'),
(select supplier_id from plant_shop_database.supplier where supplier_name = 'ООО Семь Семян'),
(select originator_id from plant_shop_database.originator where originator_name = 'BEJO ZADEN B.V.'),
	'paper pack', 0.5, 20, 75
), 
((select plant_id from plant_shop_database.plant where plant_name = 'Красивотычинник' and plant_sort='Лимонный'),
(select supplier_id from plant_shop_database.supplier where supplier_name = 'ИП Григорьев А.Ю.'),
(select originator_id from plant_shop_database.originator where originator_name = 'FlowerItaly'),
	'paper pack', 0.1, 5, 200
), 
((select plant_id from plant_shop_database.plant where plant_name = 'Лобелия многолетняя' and plant_sort='Старшип Бургунди'),
(select supplier_id from plant_shop_database.supplier where supplier_name = 'ИП Орлова Н.Д.'),
(select originator_id from plant_shop_database.originator where originator_name = 'PanAmericanSeed'),
	'paper pack', 0.1, 5, 200
);



-- Заполнение таблицы "order"
-- В качестве адресса везде использую реальные пункты выдачи OZON, ибо почему бы и нет :D

insert into plant_shop_database."order" 
(order_date, recieve_date, status, order_adress, client)
values
('2021-02-21', '2021-03-11', 'COMPLETED', 'г. Москва, Хорошёвское шоссе, 46', 'Fedor Kukushkin'),
('2021-02-21', '9999-12-31', 'CANCELED', ' г. Москва, улица Винокурова, 10, корп. 2', 'Egor Gusev'),
('2022-06-30', '2022-07-15', 'COMPLETED', 'г. Москва, Проспект Будённого, 53, стр. 2', 'Ivan Sokolov'),
('2023-09-07', '2023-09-21', 'COMPLETED', 'г. Москва, Дубнинская улица, 36', 'Pavel Belousov'),
('2024-01-12', '9999-12-31', 'CANCELED', 'г. Москва, Гостиничная улица, 9', 'Alexander Vorobyev'),
('2024-03-31', '2024-04-25', 'SHIPPED', 'г. Долгопрудный, Первомайская улица, 3A', 'Dmitri Sinitsyn'),
('2024-04-08', '2024-04-25', 'SHIPPED', 'г. Москва, поезд Черепановых, 32', 'Roman Filinov'),
('2024-04-11', '2022-04-25', 'AWAITING SHIPMENT', 'г. Москва, Дубнинская улица, 14, корп. 2', 'Andrey Solovyev'),
('2024-04-12', '2022-05-03', 'ASSEMBLING', 'г. Москва, Дубнинская улица, 36', 'Robert Snegirev'),
('2024-04-13', '2024-05-06', 'TRANSMITTED', 'г. Москва, Дубнинская улица, 36', 'Pavel Belousov');

-- Заполнение таблицы "order_product"

-- Здесь было бы лучше объединить сразу все 4 таблицы. 
-- Я сделал 1 left join вместо трёх, т.к. результат от этого не изменится.
-- Так же insert-ы пройдут отдельно, чтобы можно было визульно отличить заказы в этом разделе.

insert into plant_shop_database.order_product 
(product_id, order_id, amount)
values
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Перец сладкий' and plant_sort = 'Палермо шоколадный'),
(select order_id from plant_shop_database."order" where client='Fedor Kukushkin' and order_date='2021-02-21'),
2
), ((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Перец сладкий' and plant_sort = 'Джипси'),
(select order_id from plant_shop_database."order" where client='Fedor Kukushkin' and order_date='2021-02-21'),
2
);

insert into plant_shop_database.order_product 
(product_id, order_id, amount)
values
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Перец сладкий' and plant_sort = 'Джипси'),
(select order_id from plant_shop_database."order" where client='Egor Gusev' and order_date='2021-02-21'),
2048
);

insert into plant_shop_database.order_product 
(product_id, order_id, amount)
values
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Перец сладкий' and plant_sort = 'Айвар'),
(select order_id from plant_shop_database."order" where client='Ivan Sokolov' and order_date='2022-06-30'),
2
);

insert into plant_shop_database.order_product 
(product_id, order_id, amount)
values
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Перец сладкий' and plant_sort = 'Паланичко чудо'),
(select order_id from plant_shop_database."order" where client='Pavel Belousov' and order_date='2023-09-07'),
3
),
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Перец сладкий' and plant_sort = 'Испанский Rico'),
(select order_id from plant_shop_database."order" where client='Pavel Belousov' and order_date='2023-09-07'),
2
),
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Перец сладкий' and plant_sort = 'Lila Liebesapfel'),
(select order_id from plant_shop_database."order" where client='Pavel Belousov' and order_date='2023-09-07'),
2
),
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Морковь' and plant_sort = 'Лагуна'),
(select order_id from plant_shop_database."order" where client='Pavel Belousov' and order_date='2023-09-07'),
1
),
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Красивотычинник' and plant_sort = 'Лимонный'),
(select order_id from plant_shop_database."order" where client='Pavel Belousov' and order_date='2023-09-07'),
12
);

insert into plant_shop_database.order_product 
(product_id, order_id, amount)
values
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Перец сладкий' and plant_sort = 'Айвар'),
(select order_id from plant_shop_database."order" where client='Alexander Vorobyev' and order_date='2024-01-12'),
1
);

insert into plant_shop_database.order_product 
(product_id, order_id, amount)
values
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Перец сладкий' and plant_sort = 'Lila Liebesapfel'),
(select order_id from plant_shop_database."order" where client='Dmitri Sinitsyn' and order_date='2024-03-31'),
1
), ((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Лобелия многолетняя' and plant_sort = 'Старшип Бургунди'),
(select order_id from plant_shop_database."order" where client='Dmitri Sinitsyn' and order_date='2024-03-31'),
3
);

insert into plant_shop_database.order_product 
(product_id, order_id, amount)
values
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Перец сладкий' and plant_sort = 'Джипси'),
(select order_id from plant_shop_database."order" where client='Roman Filinov' and order_date='2024-04-08'),
2
);


insert into plant_shop_database.order_product 
(product_id, order_id, amount)
values
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Перец сладкий' and plant_sort = 'Палермо шоколадный'),
(select order_id from plant_shop_database."order" where client='Andrey Solovyev' and order_date='2024-04-11'),
2
),
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Лук Репчатый' and plant_sort = 'Эксибишен'),
(select order_id from plant_shop_database."order" where client='Andrey Solovyev' and order_date='2024-04-11'),
1
),
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Лобелия многолетняя' and plant_sort = 'Старшип Бургунди'),
(select order_id from plant_shop_database."order" where client='Andrey Solovyev' and order_date='2024-04-11'),
6
);

insert into plant_shop_database.order_product 
(product_id, order_id, amount)
values
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Красивотычинник' and plant_sort = 'Лимонный'),
(select order_id from plant_shop_database."order" where client='Robert Snegirev' and order_date='2024-04-12'),
5
),
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Перец сладкий' and plant_sort = 'Палермо шоколадный'),
(select order_id from plant_shop_database."order" where client='Robert Snegirev' and order_date='2024-04-12'),
3
);


insert into plant_shop_database.order_product 
(product_id, order_id, amount)
values
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Красивотычинник' and plant_sort = 'Лимонный'),
(select order_id from plant_shop_database."order" where client='Pavel Belousov' and order_date='2024-04-13'),
7
),
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Лук Репчатый' and plant_sort = 'Эксибишен'),
(select order_id from plant_shop_database."order" where client='Pavel Belousov' and order_date='2024-04-13'),
2
),
((select product_id from plant_shop_database.product prod
left join plant_shop_database.plant pl on prod.plant_id = pl.plant_id
where  plant_name = 'Лобелия многолетняя' and plant_sort = 'Старшип Бургунди'),
(select order_id from plant_shop_database."order" where client='Pavel Belousov' and order_date='2024-04-13'),
4
);
