-- 1 Вывести самые дорогие товары (Вид, сорт, цена)
select plant_name, plant_sort, price
from plant_shop_database.product pr
inner join plant_shop_database.plant pl
on pr.plant_id = pl.plant_id
where price = (
	select max(price) from plant_shop_database.product
);

-- 2 Вывести среднюю высоту каждого вида растений, сортировать по высоте
select distinct plant_name,
avg(plant_height) over (
	partition by plant_name 
)
as avg_height
from plant_shop_database.plant p
order by avg_height;

-- 3 Вывести сумму, которую пользователи потратили на сайте (заказы не CANCELLED)
select client,
sum(amount*price) as total
from plant_shop_database.order_product op
inner join plant_shop_database."order" o 
on o.order_id = op.order_id
inner join plant_shop_database.product p
on op.product_id = p.product_id
where status!='CANCELED'
group by client
order by client;

-- 4 Вывести все статусы, и сколько раз они встречались
select status,
count(status) as status_cnt
from plant_shop_database."order" o
group by status
order by status asc;

-- 5 Самый дешёвый перец.
select plant_name, plant_sort, price
from plant_shop_database.product pr
inner join plant_shop_database.plant pl
on pl.plant_id = pr.plant_id
where price = (
	select min(pr2.price)
	from plant_shop_database.product pr2
	inner join plant_shop_database.plant pl2
	on pl2.plant_id = pr2.plant_id
	where lower(plant_name) like '%перец%'
);

-- 6 DenseRank по высоте с разбиением по виду
select 
dense_rank() over (
	partition by plant_name
	order by plant_height
) as height_rank,
plant_name, plant_sort, plant_height
from plant_shop_database.plant p
order by plant_name;

-- 7 Rank по цене с разбиением по виду
select 
rank() over (
	partition by plant_name
	order by price
) as price_rank,
product_id, supplier_name, originator_name, country, plant_name, plant_sort, price
from plant_shop_database.plant pl
inner join plant_shop_database.product pr
on pl.plant_id = pr.plant_id 
inner join plant_shop_database.originator o 
on o.originator_id = pr.originator_id 
inner join plant_shop_database.supplier s 
on s.supplier_id = pr.supplier_id 
order by plant_name;

-- 8 Посчитать суммарную стоимость заказов Белоусова Павла на время каждого заказа.
with lots_of_joining as (
	select op.order_id,
	sum(amount*price) as order_total
	from plant_shop_database.order_product op 
	inner join plant_shop_database."order" o 
	on o.order_id = op.order_id
	inner join plant_shop_database.product p 
	on p.product_id = op.product_id
	where status != 'CANCELED'
	and client = 'Pavel Belousov'
	group by op.order_id
)
select lof.order_id, client, order_date,
sum(order_total) over (
	order by order_date
) as total_summ
from lots_of_joining lof
inner join plant_shop_database."order" o 
on o.order_id = lof.order_id;

-- 9 Расчитать среднюю сумму покупок пользователей на сайте, округлить до двух после запятой.
with lots_of_joining as (
	select client,
	sum(amount*price) as total
	from plant_shop_database.order_product op 
	inner join plant_shop_database."order" o 
	on o.order_id = op.order_id
	inner join plant_shop_database.product p 
	on p.product_id = op.product_id
	where status != 'CANCELED'
	group by client
)
select round(avg(total), 2) as avg_sum from lots_of_joining;

-- 10 Вывести ТОП-5 покупателей.
with lots_of_joining as (
	select client,
	sum(amount*price) as total
	from plant_shop_database.order_product op 
	inner join plant_shop_database."order" o 
	on o.order_id = op.order_id
	inner join plant_shop_database.product p 
	on p.product_id = op.product_id
	where status != 'CANCELED'
	group by client
)
select client, total
from lots_of_joining
order by total desc
limit 5;

-- 11 Найти самый часто используемый пункт выдачи.
with adress_count as (
select order_adress, 
count(order_id) as cnt
from plant_shop_database."order" o 
group by order_adress
)
select order_adress
from adress_count
order by cnt desc
limit 1;

--	12 Вывести число купленных товаров из каждой страны.
select country,
sum(amount) as total
from plant_shop_database.order_product op 
inner join plant_shop_database."order" o 
on o.order_id = op.order_id
inner join plant_shop_database.product p 
on p.product_id = op.product_id
inner join plant_shop_database.originator org 
on p.originator_id = org.originator_id
where status != 'CANCELED'
group by country

--	13 Допустим, что поставщик получает 10% дохода с продаж. Расчитать средний доход поставщиков с продаж и отклонение от него.
with supp_total as (
	select supplier_name,
	round(sum(amount*price)*0.1, 2) as income
	from plant_shop_database.order_product op 
	inner join plant_shop_database."order" o 
	on o.order_id = op.order_id
	inner join plant_shop_database.product p 
	on p.product_id = op.product_id
	inner join plant_shop_database.supplier s 
	on s.supplier_id = p.supplier_id 
	where status != 'CANCELED'
	group by supplier_name
)
select *,
avg(income) over () as avg_inc,
round(income * 100 / avg(income) over (), 2) - 100 as dif
from supp_total;

-- 14 Посчитать число товаров в ассортименте на страну.
select country,
count(product_id) as cnt
from plant_shop_database.product p 
inner join plant_shop_database.originator o
on o.originator_id = p.originator_id 
group by country
order by country;

-- 15 Найти доход с каждой категории растений.
select category,
round(sum(amount*price), 2) as total
from plant_shop_database.order_product op 
inner join plant_shop_database."order" o 
on o.order_id = op.order_id
inner join plant_shop_database.product p 
on p.product_id = op.product_id
inner join plant_shop_database.plant pl
on p.plant_id = pl.plant_id 
where status != 'CANCELED'
group by category;

-- 16 Найти "Любимое растение?" пользователя
with client_plant as (
select client, pl.plant_id,
round(sum(amount), 2) as total
from plant_shop_database.order_product op 
inner join plant_shop_database."order" o 
on o.order_id = op.order_id
inner join plant_shop_database.product p 
on p.product_id = op.product_id
inner join plant_shop_database.plant pl
on p.plant_id = pl.plant_id 
where status != 'CANCELED'
group by client, pl.plant_id
)
select client, plant_name, plant_sort, total
from client_plant c
join plant_shop_database.plant p
on p.plant_id = c.plant_id
where total = (
	select max(total)
	from client_plant cpCheckMax
	where cpCheckMax.client = c.client
)
order by client;

-- 17 Число проданных товаров на поставщика (0, если нет)
with supp_cnt as (
	select s.supplier_id,
	sum(amount) as cnt
	from plant_shop_database.order_product op 
	inner join plant_shop_database."order" o 
	on o.order_id = op.order_id
	inner join plant_shop_database.product p 
	on p.product_id = op.product_id
	inner join plant_shop_database.supplier s 
	on s.supplier_id = p.supplier_id 
	where status != 'CANCELED'
	group by s.supplier_id
)
select s.supplier_id, supplier_name,
coalesce((select cnt from supp_cnt sc where sc.supplier_id = s.supplier_id), 0) as sales
from plant_shop_database.supplier s 
full join supp_cnt sc
on s.supplier_id  = sc.supplier_id
order by s.supplier_id;

-- 18 Среднее число проданных товаров на поставщика, процент от этого числа(0 не идут в учёт, проставить 0 в процент)
with supp_cnt as (
	select s.supplier_id,
	sum(amount) as cnt
	from plant_shop_database.order_product op 
	inner join plant_shop_database."order" o 
	on o.order_id = op.order_id
	inner join plant_shop_database.product p 
	on p.product_id = op.product_id
	inner join plant_shop_database.supplier s 
	on s.supplier_id = p.supplier_id 
	where status != 'CANCELED'
	group by s.supplier_id
)
select s.supplier_id, supplier_name,
coalesce((select cnt from supp_cnt sc where sc.supplier_id = s.supplier_id), 0) as sales,
round(avg(cnt) over(), 1) as avg_sales,
coalesce(round(cnt*100/avg(cnt) over(), 2), 0) as sales_prc
from plant_shop_database.supplier s 
full join supp_cnt sc
on s.supplier_id  = sc.supplier_id;

-- 19 Сколько денег было возвращено клиентам (CANCELED)
-- 174 ТЫСЯЧИ РУБЛЕЙ?! КАКОЙ ИДИОТ ПРИДУМЫВАЛ ДАННЫЕ ДЛЯ ЭТОЙ ТАБЛИЦЫ?! а... ну да...
select
round(sum(amount*price), 2) as total_refund
from plant_shop_database.order_product op 
inner join plant_shop_database."order" o 
on o.order_id = op.order_id
inner join plant_shop_database.product p 
on p.product_id = op.product_id
inner join plant_shop_database.plant pl
on p.plant_id = pl.plant_id 
where status = 'CANCELED'
group by category;

-- 20 Вывести среднюю температуру посева вида растений
select distinct plant_name,
avg(sowing_temp) over(
	partition by plant_name
) as avg_temp
from plant_shop_database.plant p;