-- Понижаем цены на 20% на все товары от ИП Григорьева А.Ю.
-- Чутка криво, у меня нет истории, чтобы это не отобразилоссь на старых заказах. 
-- Было бы славно, если бы я подумал насчёт этого и вовремя сделал хотя бы SCD2.

update plant_shop_database.product p
set price = price * 0.8
where p.supplier_id = (

	select s.supplier_id 
	from plant_shop_database.supplier s
	where s.supplier_name = 'ИП Григорьев А.Ю.'
	
);



-- Отменяем все актуальные заказы, которые должны были быть доставлены на Дубнинскую улицу, 36

update plant_shop_database."order"
set recieve_date = '9999-12-31', 
	status = 'CANCELLED'
where status != 'COMPLETED' 
and order_adress = 'г. Москва, Дубнинская улица, 36';



-- Удалить все заказы Pavel Belousov 
-- (Он провёл DoS атаку на сайт :D (всего 2 раза, потому что делал это вручную (он плохой программист (даже не подумал про SCD2))))
-- Я надеюсь, что изменения этих скриптов не нужно вносить в Базу, потому что я теряю половину таблицы order_product ради этого прикола

delete from plant_shop_database.order_product op
where op.order_id in (
	select order_id 
	from plant_shop_database.order
	where client='Pavel Belousov'
);

delete from plant_shop_database."order"
where client='Pavel Belousov';



-- Я поторопился, когда пометил любелию многолетнюю, как комнатное растение

update plant_shop_database.plant
set indoor = false
where plant_name = 'Лобелия многолетняя';



-- Сдвинуть все актуальные заказы на меньше, чем 1500 руб. до 25 мая 2024
-- P.S. Оказалось, что я поставил некоторые заказы в 2024 как ожидающие получения в 2022 (X-X). Я уже это исправил.

update plant_shop_database."order" ord
set recieve_date = '2024-05-25', 
	status = 'TRANSMITTED'
where status != 'COMPLETED' and recieve_date between now() and '2024-05-25'
and (
	select sum(price*amount)
	from plant_shop_database.order_product op
	inner join plant_shop_database.product pr
	on op.product_id = pr.product_id
	where op.order_id=ord.order_id
) < 1500;



-- Удалим все перцы из актуальных заказов, которые ещё не доставлены.

delete from plant_shop_database.order_product op
where op.order_id in (

	select order_id
	from plant_shop_database."order"
	where status not in ('COMPLETED', 'CANCELLED', 'SHIPPED') -- Справедливости ради, этого условия достаточно, так что не полезу в даты
	
) and op.product_id in (
	
	select product_id as pepper_id
		from plant_shop_database.product pr
		inner join plant_shop_database.plant pl
		on pr.plant_id = pl.plant_id
		where lower(plant_name) like '%перец%'

);



-- У меня кончились идеи. Ну допустим обновим мою прошлую попытку пошутить про травку...

update plant_shop_database.supplier 
set supplier_name = 'We<3Weed Ltd.',
	supplier_email = 'wel0v3w33d@phystech.edu'
where supplier_name = 'ООО ЛегальнаяТравка';



-- Увеличить число лука в заказе Andrey Solovyev от 2024-04-11 на 7-ое число Фиббоначи (Почему бы и нет)

with recursive fib(i, a, b) as
(
	select 1, 1, 1
	union all
	select i+1, b, a+b from fib where i < 7
	
)
update plant_shop_database.order_product op
set amount = amount + (select b from fib where i=7)
where op.order_id in (
	
	select o.order_id  
	from plant_shop_database."order" o 
	where client = 'Andrey Solovyev'
	and order_date = '2024-04-11'
) 
and op.product_id in (
	
	select product_id 
	from plant_shop_database.product pr 
	inner join plant_shop_database.plant pl
	on pr.plant_id = pl.plant_id 
	where plant_name = 'Лук Репчатый'
);



-- Удаляем всех подставных поставщиков (без зависимостей)

delete from plant_shop_database.supplier s
where s.supplier_id not in (

	select distinct p.supplier_id 
	from plant_shop_database.product p
	
)